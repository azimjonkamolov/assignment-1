# Name: Assignment 1 Question 1
# Time: 09:20 09.25.2018
# Author: Azimjon Kamolov
# Contact: azimjon.6561@gmail.com
# Purpose: to calculate number of month to save an enough amount of money for downpayment 
# Created by D.python


csaving = 0                  # FOR CURRENT SAVING
dpayment = 0.25              # FOR DOWN PAYMENT
msalary = 0                 # MONTHLY SALARY
annual = int(input("Enter your annual salary: "))                                   # TO ENTER ANNUAL
save = float(input("Enter the percenteage of your salary to save, as a decimal: ")) # TO ENTER THE SAVING PERCENTAGE
home = int(input("Enter the cost of your dream home: "))                            # TO ENTER THE COST OF DREAM HOUSE
dpayment = home*dpayment    # TO CALCULATE DOWNPAYMANT
msalary = annual/12         # TO TURN ANNUAL INTO MONTHLY SALARY
month = 0                   # TO INITIALIZE MONTH AS 0
while csaving < dpayment:   # CURRENT SAVING GOES UP TO DOWN PAYMENT
    csaving += msalary*save # CURRNET SAVING KEEPS INCREASING
    month += 1              # EVERY TIME 1 IS ADDED TO MONHT

print("Number of months ", month)               # TO SHOW MONTH
print("\n")                                     # TO SEPARATE FROM THE MAIN PROGRAM

hold = input("Enter something to exit :) : ")   # TO HOLD THE SCREEN


