# Name: Assignment 1 Question 3
# Time: 09:36 09.25.2018
# Author: Azimjon Kamolov
# Contact: azimjon.6561@gmail.com
# Purpose: to find out if a number is a prime number of not
# Created by D.python


i = 2                                                                           # TO INITIALIZE  i TO 2
show = 1                                                                        # TO INITIALIZE show AS 1
num = int(input("Enter the number: "))                                          # TO ENTER INPUT TO NUM
if(num > 1):                                                                    # TO CHECK WETHER num IS HIGHER THAN 1 OR NOT
    for i in range(2, num):                                                     # i GOES UP TO num KICKING OFF FROM 2
        if(num%i == 0):                                                         # TO CHECK IF IT IS POSSIBLE
            show = 0                                                            # IF ABOVE STATEMENT IS TRUE, RUN THIS STATEMENT

    if(show == 0):                                                              # TO CHECK WETHER show IS 0 OR NOT
        print(num, " is not a prime number")                                    # IF ABOVE STATEMENT IS TRUE, RUN THIS STATEMENT
    else: # IF if IS FALSE
        print(num, "is a prime number")                                         # RUN THIS STATEMENT

else:                                                                           # IF num IS SMALLER THAN  OR EEQUAL TO 1
    print("Please enter a number higher than 1")                                # RUN THIS


print("\n")                                                                     # TO CREATE SPACE
hold = input("Enter something to exit :) : ")                                    # TO HOLD THE SCREEN