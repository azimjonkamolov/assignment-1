# Name: Assignment 1 Question 2
# Time: 09:27 09.25.2018
# Author: Azimjon Kamolov
# Contact: azimjon.6561@gmail.com
# Purpose: to save down payment with rising salary
# Created by D.python

csaving = 0                                  # FOR CURRENT SAVING
dpayment = 0.25                              # FOR DOWN PAYMENT
msalary = 0                                  # MONTHLY SALARY
annual = int(input("Enter your annual salary: ")) # TO ENTER ANNUAL
save = float(input("Enter the percenteage of your salary to save, as a decimal: "))     # TO ENTER THE SAVING PERCENTAGE
home = int(input("Enter the cost of your dream home: "))           # TO ENTER THE COST OF DREAM HOUSE
arise = float(input("Enter the rise of the salary: "))             # TO ENTER THE RISE OF THE SALARY
dpayment = home*dpayment                                           # TO CALCULATE DOWNPAYMANT
month = 0                                                          # TO INITIALIZE MONTH AS 0
num =0                                                             # TO KNOW WHEN SALARY INCREASES
while csaving < dpayment:                                          # CURRENT SAVING GOES UP TO DOWN PAYMENT
    msalary=annual/12                                              # TO TURN ANNUAL INTO MONTHLY SALARY
    csaving += msalary*save                                        # CURRNET SAVING KEEPS INCREASING
    month += 1                                                     # EVERY TIME ONE IS ADDED TO MONHT
    num +=1                                                        # IF NUM KICKS OFF WITH O HERE IT IS MULTIPLIDE BY SALAYR INCRESE FROM 0 SO IT MUST BE ADDED TO ONE FIRST
    if(num%6==0):                                                  # TO KNOW IF 6 MONTHS PASSED OR NOT
        annual=annual*arise+annual                                 # INCREASE ANNUAL

print("Number of months ", month)                                  # TO SHOW MONTH
print("\n")                                                        # TO SEPARATE FROM THE MAIN PROGRAM

hold = input("Enter something to exit :) : ")                      # TO HOLD THE SCREEN


